<?
class Dao{
    public static $instance;
    public $pagination = 0;
    public $buscar = array();

    function __construct(){
        
    }

    public static function getInstance(){
        if(isset(self::$instance)){
            self::$instance = new Dao();
        }

        return self::$instance;
    }

    public function insertData($qry){
        try{
            $sql = ConnectDB::getInstance()->prepare($qry);
            if ($sql->execute()){
                return true;
            }
        } catch(Exception $e){
            legivel($e);
            echo ' query: ' .$qry;
            return false;
        }
    }
    
    public function contador($qry){
        try{
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            return $sql;
        } catch(Exception $e){
            legivel($e);
            echo ' query: ' .$qry;
            return false;
        }
    }

    public function updateData($qry){
        try{
            $sql = ConnectDB::getInstance()->prepare($qry);
            return $sql->execute();
        } catch(Exception $e){
            legivel($e);
            echo ' query: ' .$qry;
            return false;
        }
    }

    public function deletar($qry){
        try{
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            return $sql->rowCount();
        } catch(Exception $e){
            legivel($e);
            echo ' query: ' .$qry;
            return false;
        }
    }

    public function listarData($qry, $unique=false){
        try{
            if($this->pagination > 0){
                $sql = ConnectDB::getInstance()->prepare($qry);
                $sql->execute();
                $totalResults = $sql->rowCount();
                $totalPaginas = ceil($totalResults / $this->pagination);
                $qry = $qry.$this->limitPagination($this->pagination);
            }

            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            if(!$unique){
                $resultSet = $sql->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $resultSet = $sql->fetch(PDO::FETCH_ASSOC);
            }

            return array(
                'resultSet' => $resultSet,
                'totalResults' => ($totalResults ? $totalResults : $sql->rowCount()),
                'qtPaginas' => ($totalPaginas ? $totalPaginas : 0)
            );
        }catch(Exception $e){
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }

    public function limitPagination($qtRegistros){
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $inicio = ($qtRegistros * $pagAtual) - $qtRegistros;

        return " LIMIT ".$inicio.", ".$qtRegistros;
    }

    public function renderPagination($qtPaginas){
        global $url_site;
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $url = $url_site.$_GET['page'].'/'.trataUrl($_GET['b']);

        $estrutura = '
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link text-dark" href="'.$url.'pagina/1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';

                $incremento = ($pagAtual > 3 ? $pagAtual - 2 : 1);

                for ($i=$incremento; $i < $pagAtual; $i++) { 
                    $estrutura .= '<li class="page-item"><a class="page-link text-dark" href="'.$url.'pagina/'.$i.'">'.$i.'</a></li>';
                }            

                $estrutura .= '<li class="page-item active"><a class="page-link bg-dark text-light border border-dark" href="'.$url.'pagina/'.$pagAtual.'">'.$pagAtual.'</a></li>';
                
                if($pagAtual <= $qtPaginas){
                    $calculaInc = $qtPaginas - $pagAtual;
                    $incremento = ($calculaInc > 2 ? 2 : $calculaInc);

                    for ($i=$pagAtual+1; $i <= $pagAtual+$incremento; $i++) { 
                        $estrutura .= '<li class="page-item"><a class="page-link text-dark" href="'.$url.'pagina/'.$i.'">'.$i.'</a></li>';
                    }
                }

                $estrutura .= ' 
                <li class="page-item"><a class="page-link text-dark" href="'.$url.'pagina/'.$qtPaginas.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
            </ul>
        </nav>';

        return $estrutura;
    }

}

?>