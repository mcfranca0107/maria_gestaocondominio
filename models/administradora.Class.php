<?
Class Administradora extends Dao{
    protected $id;

    function __construct(){

    }
    
    function setAdm($dados){
        $values = '';
        $sql = 'INSERT INTO ap_administradora (';

        foreach($dados as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql); 
    }

    function getAdm($id=null, $unique=false){
        $qry = 'SELECT * FROM ap_administradora';

        $contaTermos = count($this->buscar);

        if($contaTermos > 0){
            
            $contador = 0;
            foreach($this->buscar as $field => $termo){
                if($contador == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $contador++;
                }
                
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                    break;
                
                    default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                    break;
                }   
            }
            $qry = rtrim($qry, ' AND');
        }

        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique=true;
        }
        return $this->listarData($qry,$unique);
    }

    function editAdm($dados){
        $sql = 'UPDATE ap_administradora SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaAdm($id){
        $sql = 'DELETE FROM `ap_administradora` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>