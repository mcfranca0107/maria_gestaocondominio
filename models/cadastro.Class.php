<?

Class Cadastro extends CadUnidades{
    protected $id;

    function __construct(){

    }
    
    function setMorador($dados){
        $values = '';
        $sql = 'INSERT INTO ap_moradores (';

        foreach($dados as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql); 
    }

    function getMorador($id=null,$unique=false){
        $qry = 'SELECT mor.id,
                mor.from_condominio,
                mor.from_bloco,
                mor.from_unidade,
                cond.nomeCond,
                bloco.nomeBloco,
                uni.numUnidade,
                mor.nome,
                mor.cpf,
                mor.telefone,
                mor.email,
                mor.dataCadastro
            FROM ap_moradores mor
            INNER JOIN ap_condominio cond ON cond.id = mor.from_condominio
            INNER JOIN ap_blocos bloco ON bloco.id = mor.from_bloco
            INNER JOIN ap_unidades uni ON uni.id = mor.from_unidade';

        $contaTermos = count($this->buscar);

        if($contaTermos > 0){
            
            $contador = 0;
            foreach($this->buscar as $field => $termo){
                if($contador == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $contador++;
                }
                
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                    break;
                
                    default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                    break;
                }   
            }
            $qry = rtrim($qry, ' AND');
        }
        
        if($id){
            $qry .= ' WHERE mor.id='.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function editMorador($dados){
        $sql = 'UPDATE ap_moradores SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaMorador($id){
        $sql = 'DELETE FROM `ap_moradores` WHERE `id`='.$id;

        return $this->deletar($sql);
    }
}

?>