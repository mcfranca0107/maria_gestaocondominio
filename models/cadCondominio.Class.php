<?

Class CadCondominio extends Administradora{
    protected $id;
    
    function __construct(){
        
    }

    function setCondominio($dadosCond){
        $values = '';
        $sql = 'INSERT INTO ap_condominio (';

        foreach($dadosCond as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getCondominio($id=null, $unique=false){
        $qry = 'SELECT cond.id,
                cond.from_administradora,
                adm.nomeAdm,
                cond.nomeCond,
                cond.qtdeBlocos,
                cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.estado, cond.cep
            FROM ap_condominio cond
            INNER JOIN ap_administradora adm ON adm.id = cond.from_administradora';
  
        $contaTermos = count($this->buscar);
        if($contaTermos > 0){
            
            $contador = 0;
            foreach($this->buscar as $field => $termo){
                if($contador == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $contador++;
                }
                
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                    break;
                
                    default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                    break;
                }   
            }
            $qry = rtrim($qry, ' AND');
        }

        if($id){
            $qry .= ' WHERE cond.id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function editCondominio($dadosCond){
        
        $sql = 'UPDATE ap_condominio SET ';

        foreach($dadosCond as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosCond['editar'];

        return $this->updateData($sql);
    }

    function deletaCondominio($id){

        $sql = 'DELETE FROM `ap_condominio` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>