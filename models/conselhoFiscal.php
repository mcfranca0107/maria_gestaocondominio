<?

Class conselhoFiscal extends CadCondominio{
    protected $id;

    function __construct(){

    }
    
    function setConselho($dados){
        $values = '';
        $sql = 'INSERT INTO ap_conselho (';

        foreach($dados as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getConselho($id=null, $unique=false){
        $qry = 'SELECT cons.id,
                cons.from_condominio,
                cond.nomeCond,
                cons.nome,
                cons.cpf,
                cons.telefone,
                cons.funcao
            FROM ap_conselho cons
            INNER JOIN ap_condominio cond ON cond.id = cons.from_condominio';
            
        if($id){
            $qry .= ' WHERE cons.id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function editConselho($dados){
        $sql = 'UPDATE ap_conselho SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaConselho($id){
        $sql = 'DELETE FROM `ap_conselho` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>