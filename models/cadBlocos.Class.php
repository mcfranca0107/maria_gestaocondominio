<?
Class cadBlocos extends CadCondominio{
    protected $id;

    function __construct(){
        
    }

    function setBlocos($dadosBlocos){
        $values = '';
        $sql = 'INSERT INTO ap_blocos (';

        foreach($dadosBlocos as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getBlocos($id=null, $unique=false){
        $qry = 'SELECT blo.id,
                blo.from_condBloco,
                cond.nomeCond,
                blo.nomeBloco,
                blo.qtdeAndares,
                blo.qtdeUni
            FROM ap_blocos blo
            INNER JOIN ap_condominio cond ON cond.id = blo.from_condBloco';
        
        $contaTermos = count($this->buscar);

        if($contaTermos > 0){
            
            $contador = 0;
            foreach($this->buscar as $field => $termo){
                if($contador == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $contador++;
                }
                
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                    break;
                
                    default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                    break;
                }   
            }
            $qry = rtrim($qry, ' AND');
        }
            
        if($id){
            $qry .= ' WHERE blo.id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function editBloco($dadosBlocos){
        $sql = 'UPDATE ap_blocos SET ';

        foreach($dadosBlocos as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosBlocos['editar'];

        return $this->updateData($sql);
    }

    function deletaBloco($id){
        $sql = 'DELETE FROM `ap_blocos` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeBloco FROM ap_blocos WHERE from_condBloco = '.$cond;
        return $this->listarData($qry);
    }

}

?>