<?
Class CadUnidades extends cadBlocos{
    protected $id;

    function __construct(){
        
    }

    function setDadosUni($dadosUni){
        $values = '';
        $sql = 'INSERT INTO ap_unidades (';

        foreach($dadosUni as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getDadosUni($id=null, $unique=false){
        $qry = 'SELECT uni.id,
                uni.from_blocoUni,
                uni.from_condBloco,
                cond.nomeCond,
                bloco.nomeBloco,
                uni.numUnidade,
                uni.metroUnidade,
                uni.qtdeVagas,
                uni.dataCadastro
            FROM ap_unidades uni
            INNER JOIN ap_condominio cond ON cond.id = uni.from_condBloco
            INNER JOIN ap_blocos bloco ON bloco.id = uni.from_blocoUni';

        if($id){
            $qry .= ' WHERE uni.id= '.$id;
            $unique=true;
        }
        return $this->listarData($qry,$unique);
    }

    function editUnidade($dadosUni){
        $sql = 'UPDATE ap_unidades SET ';

        foreach($dadosUni as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosUni['editar'];

        return $this->updateData($sql);
    }

    function deletaUnidade($id){
        $sql = 'DELETE FROM `ap_unidades` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

    function getUnidadeFromBloco($id){
        $qry = 'SELECT id, numUnidade FROM ap_unidades WHERE from_blocoUni = '.$id;
        return $this->listarData($qry);
    }

}

?>