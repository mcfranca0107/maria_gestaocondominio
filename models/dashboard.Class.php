<?
Class Dashboard extends Dao{

    function __construct(){    
    }

    function contMorador(){
        $total = "SELECT 
                cond.nomeCond,
                COUNT(mor.id) AS totalMoradores 
                FROM ap_moradores mor
                LEFT JOIN ap_condominio cond ON cond.id = mor.from_condominio
                GROUP BY from_condominio";
        $estrutura='<p>';
        foreach($this->contador($total) as $valor){
            $estrutura .= $valor['nomeCond']." <i class='bi bi-arrow-right-short' style='font-size: 17px'></i> ".$valor['totalMoradores']."<br>";
        }
        $estrutura .= "</p>";

        return $estrutura;
    }

    function ultimasCinco(){
        $qry = "SELECT nomeAdm FROM ap_administradora ORDER BY id DESC LIMIT 5";
        $estrutura = '<ul>';

        foreach($this->contador($qry) as $valor){
            $estrutura .= '<li>'.$valor['nomeAdm'].'</li>';
        }

        $estrutura .= '</ul>';

        return $estrutura;
    }

    function total(){
        $qry = "SELECT COUNT(adm.id) AS totalAdm,
            (SELECT COUNT(id) FROM ap_condominio) AS totalCond,
            (SELECT COUNT(id) FROM ap_blocos) AS totalBlocos,
            (SELECT COUNT(id) FROM ap_unidades) AS totalUni
            FROM ap_administradora adm";

        $s = $this->listarData($qry);

        $estrutura = '<h6 class="bg-light mr-5 p-4">Total de administradoras: <span class="badge bg-light text-muted" style="font-size: 20px">'.$s['resultSet'][0]['totalAdm'].'</span></h6>';
        $estrutura .= '<h6 class="bg-light mr-5 p-4">Total de condomínios: <span class="badge bg-light text-muted" style="font-size: 20px">'.$s['resultSet'][0]['totalCond'].'</span></h6>';
        $estrutura .= '<h6 class="bg-light mr-5 p-4">Total de blocos: <span class="badge bg-light text-muted" style="font-size: 20px">'.$s['resultSet'][0]['totalBlocos'].'</span></h6>';
        $estrutura .= '<h6 class="bg-light p-4">Total de unidades: <span class="badge bg-light text-muted" style="font-size: 20px">'.$s['resultSet'][0]['totalUni'].'</span></h6>';

        return $estrutura;
    }
}

?>