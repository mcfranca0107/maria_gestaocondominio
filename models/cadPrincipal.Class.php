<?
Class cadPrincipal extends Dao{
    protected $id;

    function __construct(){ }

    function setUsuario($dados){
        $values = '';
        $sql = 'INSERT INTO ap_usuarios (';

        foreach($dados as $ch=>$value){ 
            if($ch != 'confSenha'){
                $sql .= '`'.$ch.'`, ';
                if($ch=='senha'){
                    $value=md5($value);
                }
                $values .= "'".$value."', ";
            }
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql); 
    }

    function getUsuario($id=null, $unique=false){
        $qry = "SELECT id, nome, usuario, dataCadastro FROM ap_usuarios";
        
        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique = true;
        }

        return $this->listarData($qry,$unique);
    }

    function editUsuario($dados){
        $sql = 'UPDATE ap_usuarios SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar' && $ch != 'confSenha'){
                if($ch=='senha'){
                    $value=md5($value);
                }
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaUsuario($id){
        $sql = 'DELETE FROM `ap_usuarios` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

    function userExistis($user){
        $qry = "SELECT usuario FROM ap_usuarios WHERE usuario = '".$user."'";
        return $this->listarData($qry,true);
    }

}

?>