<?
require "uteis.php";

$user = new Restrito();
if(!$user->acesso()){
    header("Location: ".$url_site."login.php");
}
if(($_GET['page']) == 'logout'){
    header('Location: '.$url_site.'login.php');
}

$nome = explode(' ',$_SESSION['USUARIO']['nome'])

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Gestão de Clientes</title>
</head>

<body>
      
    <nav class="navbar navbar-expand-lg bg-dark mb-4">
        <a class="navbar-brand" href="<?=$url_site?>inicio"><i class="bi bi-person-bounding-box ml-4" style="font-size: 3rem; color: #fff"></i> <span class="text-white font-weight-bold pl-3"> GESTÃO DE CLIENTES </span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse justify-content-end mr-5" id="navbarNav">
            <ul class="navbar-nav float-right">
                <li class="nav-item pt-2 mr-1">
                    <a class="nav-link text-white btn btn-dark" href="<?=$url_site?>inicio">Home</a>       
            </ul>

            <div class="dropdown pt-2 mr-1">
                <a class="btn btn-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">
                    Cadastros
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="<?=$url_site?>cadAdm">Administradoras</a>
                    <a class="dropdown-item" href="<?=$url_site?>cadCondominio">Condomínios</a>
                    <a class="dropdown-item" href="<?=$url_site?>cadBlocos">Blocos</a>
                    <a class="dropdown-item" href="<?=$url_site?>cadUnidades">Unidades</a>
                    <a class="dropdown-item" href="<?=$url_site?>cadastro">Moradores</a>
                    <a class="dropdown-item" href="<?=$url_site?>cadastroPrincipal">Usuários</a>
                    <a class="dropdown-item" href="<?=$url_site?>cadConselho">Conselho</a>
                </div>
            </div>

            <div class="dropdown pt-2">
                <a class="btn btn-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">
                    Consultas
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="<?=$url_site?>consultaAdm">Administradoras</a>
                    <a class="dropdown-item" href="<?=$url_site?>consultaCond">Condomínios</a>
                    <a class="dropdown-item" href="<?=$url_site?>consultaBlocos">Blocos</a>
                    <a class="dropdown-item" href="<?=$url_site?>consultaUni">Unidades</a>
                    <a class="dropdown-item" href="<?=$url_site?>consulta">Moradores</a>
                    <a class="dropdown-item" href="<?=$url_site?>consultaUser">Usuários</a>
                    <a class="dropdown-item" href="<?=$url_site?>consultaConselho">Conselho</a>
                </div>
            </div>

            
            <small class="text-light pl-5 pt-2">Olá <?=$nome[0]?>, seja bem vindo(a).</small>
            <a class="nav-link pl-4" href="<?=$url_site?>config"><i class="bi bi-gear-fill" style="font-size: 20px; color: #fff"></i></a>
            <a class="nav-link pl-1" href="<?=$url_site?>logout"><i class="bi bi-box-arrow-right" style="font-size: 20px; color: #fff"></i></a>         
        </div>
    </nav>

    <main class="container">
    <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "controllers/inicio.php";
                require "views/inicio.php";
            break;
            
            default:
                require 'controllers/'.$_GET['page'].'.php';
                require 'views/'.$_GET['page'].'.php';
            break;
        }
    ?>

    <div class="empurra"></div>
    </main>

    <footer class="bg-dark mt-5">
        <div class="copyright">
            <span>&copy; Todos os direitos reservados.</span>
            <small class="versionControl"> - V 1.0</small>
        </div>

        <div class="suporte">
            <span>Suporte: </span><a href="#"><i class="bi bi-whatsapp"></i> (47) 99776-0344</a>
        </div>
        
        <div class="marca">
            <span>Gestão de Clientes </span>
            <i class="bi bi-person-bounding-box ml-2" style="font-size: 1rem; color: #fff"></i>
        </div>
    </footer>

    <script> var url_site = '<?=$url_site?>';</script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.bundle.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/jquery.mask.min.js"></script>
    <script src="<?=$url_site?>js/app.js?v=<?=rand(0,9999)?>"></script>
</body>

</html>