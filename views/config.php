<div class="row">
    <div class="col 12 text-center">
        <p class="configPag"> Ainda estamos trabalhando nessa página e logo estará pronta para você usar! </p>
        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
        </div>
    </div>
</div>