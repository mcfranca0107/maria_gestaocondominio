<h1 class="text-center mb-4">Consulta do Conselho Fiscal</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadConselho" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>
        <table class="table text-center" id="listaConselho">
            <thead>
                <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Nome</th>
                    <th scope="col">CPF</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Função</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $cons){
                ?>
                    
                    <tr data-id="<?=$cons['id']?>">
                        <td><?=$cons['nomeCond']?></td>
                        <td><?=$cons['nome']?></td>
                        <td><?=$cons['cpf']?></td>
                        <td><?=$cons['telefone']?></td>
                        <td><?=$cons['funcao']?></td>
                        <td><a href="<?=$url_site?>cadConselho/id/<?=$cons['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$cons['id']?>" class="text-dark removerConselho"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="6" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>

            </tbody>
        </table>
        <div class="col-sm-12">
            <?=$paginacao?>
        </div>
    </div>
</div>