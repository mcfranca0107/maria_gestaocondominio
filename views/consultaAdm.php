<h1 class="text-center mb-4">Consulta das Administradoras</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadAdm" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
            <input type="hidden" name="page" value="consultaAdm">
            <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nomeAdm]">
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" disabled>Buscar</button>
            <a class="btn btn-outline-danger my-2 my-sm-0 ml-2" href="<?=$url_site?>consultaAdm">Limpar</a>
        </form>

        <table class="table text-center" id="listaAdm">
            <thead>
                <tr>
                    <th scope="col">Administradora</th>
                    <th scope="col">Quantidade de Condomínios</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $adm){
                ?>
                    
                    <tr data-id="<?=$adm['id']?>">
                        <td><?=$adm['nomeAdm']?></td>
                        <td><?=$adm['qtdeCond']?></td>
                        <td><a href="<?=$url_site?>cadAdm/id/<?=$adm['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$adm['id']?>" class="text-dark removerAdm"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="3" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>

            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$paginacao?>
        </div>
    </div>
</div>