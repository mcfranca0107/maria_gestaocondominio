<style>
    .dashboardInicio h6{
        border: 1px solid black;
        border-radius: 20px;
    }
</style>

<div class="row dashboardInicio">
    <?=$contagem?>
</div>

<div class="row mt-5 mb-5 text-center">
    <div class="col-sm-6 d-inline-block">
        <div class="card position-relative" style="width: 70%; left: 13%">
            <img src="imagens/condMoradores.png" class="card-img-top img-thumbnail" alt="...">
            <div class="card-body">
            <h5 class="card-title">Quantidade de moradores por condomínio</h5>
            <p class="card-text"><?=$contador?></p>
            <a href="index.php?page=consulta" class="btn btn-dark">Consultar moradores</a>
            </div>
        </div>
    </div>

    <div class="col-sm-6 d-inline-block align-top">
        <div class="card position-relative" style="width: 80%; left: 5%; padding-bottom:17px;">
            <img src="imagens/administradoras-de-condominios.jpg" class="card-img-top img-thumbnail" alt="...">
            <div class="card-body">
            <h5 class="card-title">Últimas 5 administradoras cadastradas</h5>
            <p class="card-text"><?=$listagem?>
            </p>
            <a href="index.php?page=consulta" class="btn btn-dark">Consultar administradoras</a>
            </div>
        </div>
    </div>
</div>

