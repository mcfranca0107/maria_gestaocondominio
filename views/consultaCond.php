<h1 class="text-center mb-4">Consulta dos condomínios</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadCondominio" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
            <input type="hidden" name="page" value="consultaCond">
            <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nomeCond]">
            <select name="b[from_administradora]" class="custom-select mr-2 pr-5 termo2">
                <option value="">Buscar por administradora</option>
                <?  
                    foreach($listAdm['resultSet'] as $adm){?>
                        <option value="<?=$adm['id']?>"> <?=$adm['nomeAdm']?></option> 
                    <?}?>
            </select>
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" disabled>Buscar</button>
            <a class="btn btn-outline-danger my-2 my-sm-0 ml-2" href="<?=$url_site?>consultaCond">Limpar</a>
        </form>

        <table class="table text-center" id="listaCond">
            <thead>
                <tr>
                    <th scope="col">Administradora</th>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Quantidade de Blocos</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $cond){
                ?>
                    
                    <tr data-id="<?=$cond['id']?>">
                        <td><?=$cond['nomeAdm']?></td>
                        <td><?=$cond['nomeCond']?></td>
                        <td><?=$cond['qtdeBlocos']?></td>
                        <td>Rua <?=$cond['logradouro']?>, <?=$cond['numero']?> - <?=$cond['bairro']?> - <?=$cond['cidade']?>/<?=$cond['estado']?> - <?=$cond['cep']?>
                        </td>
                        <td><a href="<?=$url_site?>cadCondominio/id/<?=$cond['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$cond['id']?>" class="text-dark removerCond"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>

            </tbody>
        </table>
        <div class="col-sm-12">
            <?=$paginacao?>
        </div>
    </div>
</div>