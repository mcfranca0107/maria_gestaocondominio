<div class="row">

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>cadAdm" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center" style="width:100%">Cadastrar Administradoras</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>consultaAdm" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center" style="width:100%">Consultar Administradoras</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>cadConselho" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar conselho fiscal</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>consultaConselho" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar conselho fiscal</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>cadCondominio" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar condomínios</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>consultaCond" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar condomínios</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>cadBlocos" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar blocos</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>consultaBlocos" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar blocos</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>cadUnidades" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar unidades</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="<?$url_site?>consultaUni" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar unidades</a>
    </div>
</div>
