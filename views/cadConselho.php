<h1 class="text-center mb-5">Cadastros do Conselho Fiscal</h1>
<div class="row">
    <div class="col-12">
        <form action="" method="post" id="formConselho">
            <div class="form-group col-md-6">
                <label for="name">Nome*</label>
                <input type="text" name="nome" class="form-control" id="name" aria-describedby="name" value="<?=$conselho['nome']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="cpf">CPF*</label>
                <input type="text" name="cpf" class="form-control" id="cpf" placeholder="000.000.000-00" value="<?=$conselho['cpf']?>" required>
            </div>
            
            <div class="form-group col-12 col-md-6">
                <label for="telefone">Telefone</label>
                <input type="text" name="telefone" class="form-control" id="telefone" aria-describedby="telefone" value="<?=$conselho['telefone']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="funcao">Função*</label>

                <select name="funcao" id="funcao" class="custom-select">
                    <option value="">Select</option>
                    <option value="Síndico" <?=(($conselho['funcao'] == "sindico") ? 'selected' : '') ?>>Síndico</option>
                    <option value="Subsíndico" <?=(($conselho['funcao'] == "subsindico") ? 'selected' : '') ?>>Subsíndico</option>
                    <option value="Conselheiro" <?=(($conselho['funcao'] == "conselheiro") ? 'selected' : '') ?>>Conselheiro</option>
                </select>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="from_condominio">Condomínio*</label>
                
                <select name="from_condominio" id="from_condominio" class="custom-select">
                    <option value="">Select</option>
                    <?
                    foreach($cond as $ch=>$valor){?>
                        <option value="<?=$valor['id']?>" <?=($valor['id'] == $conselho['from_condominio'] ? 'selected' : '') ?>> <?=$valor['nomeCond']?></option> 
                    <?}?>
                </select>
            </div>

            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <button type="submit" class="btn btn-dark btnEnviar col-12 col-sm-1 ml-3 mb-3">Enviar</button>
            <a href="<?=$url_site?>consultaConselho" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar cadastros"><i class="bi bi-journal-text" style="font-size: 2rem"></i></a>
        </form>
    </div>
</div>