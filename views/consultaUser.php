<h1 class="text-center mb-4">Consulta de todos os cadastros</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadastroPrincipal" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>
        <table class="table text-center" id="listaUser">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Usuário</th>
                    <th scope="col">Data do cadastro</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $user){
                ?>                    
                    <tr data-id="<?=$user['id']?>">
                        <td><?=$user['nome']?></td>
                        <td><?=$user['usuario']?></td>
                        <td><?=$user['dataCadastro']?></td>
                        <td><a href="index.php?page=cadastroPrincipal&id=<?=$user['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$user['id']?>" class="text-dark removerUsuario"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="4" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>

            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$paginacao?>
        </div>
    </div>
</div>