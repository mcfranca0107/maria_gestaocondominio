<h1 class="mb-4 mt-3 text-center">Cadastro de Unidades</h1>

<div class="row">
    <div class="col-12">
        <form action="" method="post" id="formUnidades">

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="from_condBloco" style="width: 100%; text-align: center">Condomínio*</label>
                <div class="col-12 col-md-12">
                    <select name="from_condBloco" id="from_condBloco" class="custom-select fromCondominio" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?
                        foreach($cond as $ch=>$valor){?>
                            <option value="<?=$valor['id']?>" <?=($valor['id'] == $unidades['from_condBloco'] ? 'selected' : '') ?>> <?=$valor['nomeCond']?></option> 
                        <?}?>

                    </select>
                </div>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="from_blocoUni" style="width: 100%; text-align: center">Bloco*</label>
                <div class="col-12 col-md-12">
                    
                    <select name="from_blocoUni" id="from_blocoUni" class="custom-select fromBloco" style="width:30%; margin: 0 23rem">
                    
                        <?if($_GET['id']){
                            $blocos = $listaUnidades->getBlocoFromCond($unidades['from_condominio']);
                            foreach($blocos['resultSet'] as $bloco){?>
                                <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $unidades['from_blocoUni'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                        <?}}?>
                    
                    </select>
                </div>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%; ">
                <label for="numUnidade">Número da unidade*</label>
                <input type="text" name="numUnidade" class="form-control" id="numUnidade" value="<?=$unidades['numUnidade']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center " style="width: 100%;">
                <label for="metroUnidade">Metragem da unidade*</label>
                <input type="text" name="metroUnidade" class="form-control" id="metroUnidade" value="<?=$unidades['metroUnidade']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="qtdeVagas">Quantidade de vagas na garagem*</label>
                <input type="text" name="qtdeVagas" class="form-control" id="qtdeVagas" value="<?=$unidades['qtdeVagas']?>" style="width:50%; margin: 0 270px" require>
            </div>
        
            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <div class="col-12 text-center">
                <button type="submit" class="btn btn-dark btnEnviar col-12 ml-3 mb-3" style="width: 10%;">Enviar</button>
                <a href="<?=$url_site?>consultaUni" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar blocos"><i class="bi bi-journal-text" style="font-size: 2rem"></i></a>
            </div>
        </form>
    </div>
</div>