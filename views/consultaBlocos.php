<h1 class="text-center mb-4">Consulta de Blocos</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadBlocos" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
            <input type="hidden" name="page" value="consultaBlocos">
            <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nomeBloco]">
            <select name="b[from_condBloco]" class="custom-select mr-2 pr-5 termo2">
                <option value="">Buscar por condomínio</option>
                <?  
                    foreach($listCond['resultSet'] as $condominios){?>
                        <option value="<?=$condominios['id']?>"> <?=$condominios['nomeCond']?></option> 
                    <?}?>
            </select>
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" disabled>Buscar</button>
            <a class="btn btn-outline-danger my-2 my-sm-0 ml-2" href="<?=$url_site?>consultaBlocos">Limpar</a>
        </form>

        <table class="table text-center" id="listaBloco">
            <thead>
                <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Nome do Bloco</th>
                    <th scope="col">Quantidade de Andares</th>
                    <th scope="col">Unidades por Andar</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>
                <?
                foreach($result['resultSet'] as $bloco){
                ?>
                    
                    <tr data-id="<?=$bloco['id']?>">
                        <td><?=$bloco['nomeCond']?></td>
                        <td><?=$bloco['nomeBloco']?></td>
                        <td><?=$bloco['qtdeAndares']?></td>
                        <td><?=$bloco['qtdeUni']?></td>
                        <td><a href="<?=$url_site?>cadBlocos/id/<?=$bloco['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$bloco['id']?>" class="text-dark removerBloco"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>

            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$paginacao?>
        </div>
    </div>
</div>