<h1 class="text-center mb-4">Consulta das Unidades</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadUnidades" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <table class="table text-center" id="listaUni">
            <thead>
                <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Bloco</th>
                    <th scope="col">Número da Unidade</th>
                    <th scope="col">Metragem</th>
                    <th scope="col">Vagas na Garagem</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $uni){
                ?>
                    
                    <tr data-id="<?=$uni['id']?>">
                        <td><?=$uni['nomeCond']?></td>
                        <td><?=$uni['nomeBloco']?></td>
                        <td><?=$uni['numUnidade']?></td>
                        <td><?=$uni['metroUnidade']?></td>
                        <td><?=$uni['qtdeVagas']?></td>
                        <td><a href="<?=$url_site?>cadUnidades/id/<?=$uni['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$uni['id']?>" class="text-dark removerUni"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="6" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>

            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$paginacao?>
        </div>
    </div>
</div>