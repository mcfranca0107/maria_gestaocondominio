
<h1 class="text-center mb-5">Consulta dos moradores</h1>

<div class="row">
    <div class="col-12">
        
        <span class="float-right mr-4 mb-1">
            <a href="<?=$url_site?>cadastro" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
            <input type="hidden" name="page" value="consulta">
            <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nome]">
            <select name="b[from_condominio]" class="custom-select mr-2 pr-5 termo2">
                <option value="">Buscar por condomínio</option>
                <?  
                    foreach($listCond['resultSet'] as $condominios){?>
                        <option value="<?=$condominios['id']?>"> <?=$condominios['nomeCond']?></option> 
                    <?}?>
            </select>
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" disabled>Buscar</button>
            <a class="btn btn-outline-danger my-2 my-sm-0 ml-2" href="<?=$url_site?>consulta">Limpar</a>
        </form>

        <table class="table text-center" id="listaMorador">
            <thead>
                <tr>
                    <th scope="col">Nome</td>
                    <th scope="col">CPF</td>
                    <th scope="col">E-mail</td>
                    <th scope="col">Telefone</td>
                    <th scope="col">Condomínio</td>
                    <th scope="col">Bloco</td>
                    <th scope="col">Unidade</td>
                    <th scope="col" title="Data Cadastro"><i class="bi bi-calendar-plus" style="font-size: 25px;"></i></td>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>
            
            <tbody>

                <?
                    foreach($result['resultSet'] as $m){
                ?>

                    <tr data-id="<?=$m['id']?>">
                        <td><?=$m['nome']?></td>
                        <td><?=$m['cpf']?></td>
                        <td><?=$m['email']?></td>
                        <td><?=$m['telefone']?></td>
                        <td><?=$m['nomeCond']?></td>
                        <td><?=$m['nomeBloco']?></td>
                        <td><?=$m['numUnidade']?></td>
                        <td><?=dateFormat($m['dataCadastro'])?></td>
                        <td><a href="<?=$url_site?>cadastro/id/<?=$m['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$m['id']?>" class="text-dark removerMorador"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>
                <tr>
                    <td colspan="9" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=$totalRegistros?></td>
                </tr>
            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$paginacao;?>
        </div>
    </div>
</div>