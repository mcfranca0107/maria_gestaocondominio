<h1 class="mb-4 mt-3 text-center">Realizar Cadastro</h1>

<div class="row">
    <div class="col-12">
        <form action="" method="post" id="formCadastro">

            <div class="form-group col-12 text-center" style="width: 100%; ">
                <label for="nome">Nome*</label>
                <input type="text" name="nome" class="form-control" id="nomeBloco" value="<?=$user['nome']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center " style="width: 100%;">
                <label for="usuario">Usuário*</label>
                <input type="text" name="usuario" class="form-control" id="usuario" value="<?=$user['usuario']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="senha">Senha*</label>
                <input type="password" name="senha" class="form-control" id="senha" value="<?=$user['senha']?>" style="width:20%; margin: 0 27rem" require>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="senha">Confirmar senha*</label>
                <input type="password" name="confSenha" class="form-control" id="senha" value="" style="width:20%; margin: 0 27rem" require>
            </div>

            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>
                
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-dark btnEnviar col-12 mt-2 mb-3" style="width: 10%;">Cadastrar</button>
                <a href="<?=$url_site?>consultaUser" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar blocos"><i class="bi bi-journal-text mt-2" style="font-size: 2rem"></i></a>
            </div>
        </form>
    </div>
</div>