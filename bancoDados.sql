-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para ap_sistema
DROP DATABASE IF EXISTS `ap_sistema`;
CREATE DATABASE IF NOT EXISTS `ap_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ap_sistema`;

-- Copiando estrutura para tabela ap_sistema.ap_administradora
DROP TABLE IF EXISTS `ap_administradora`;
CREATE TABLE IF NOT EXISTS `ap_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(255) NOT NULL DEFAULT '',
  `qtdeCond` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_administradora: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_administradora` DISABLE KEYS */;
INSERT INTO `ap_administradora` (`id`, `nomeAdm`, `qtdeCond`) VALUES
	(1, 'Ac Adm', 4),
	(2, 'Metrópole', 4),
	(3, 'Moradia ADM', 4),
	(18, 'ADM AP CONTROL', 4),
	(19, 'Conds 123', 4),
	(20, 'Moradia ADM', 4),
	(22, 'Adm Milheis', 9);
/*!40000 ALTER TABLE `ap_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_sistema.ap_blocos
DROP TABLE IF EXISTS `ap_blocos`;
CREATE TABLE IF NOT EXISTS `ap_blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condBloco` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `qtdeAndares` int(11) NOT NULL DEFAULT 0,
  `qtdeUni` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_ap_blocos_ap_condominio` (`from_condBloco`),
  CONSTRAINT `FK_ap_blocos_ap_condominio` FOREIGN KEY (`from_condBloco`) REFERENCES `ap_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_blocos: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_blocos` DISABLE KEYS */;
INSERT INTO `ap_blocos` (`id`, `from_condBloco`, `nomeBloco`, `qtdeAndares`, `qtdeUni`) VALUES
	(1, 1, 'Bloco A', 5, 4),
	(2, 1, 'Bloco B', 3, 4),
	(3, 1, 'Bloco C', 6, 5),
	(4, 1, 'Bloco D', 3, 2),
	(5, 1, 'Bloco E', 2, 5),
	(6, 2, 'Bloco A', 4, 3),
	(7, 2, 'Bloco B', 3, 2),
	(8, 2, 'Bloco C', 4, 3),
	(28, 3, 'Bloco teste', 5, 4);
/*!40000 ALTER TABLE `ap_blocos` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_sistema.ap_condominio
DROP TABLE IF EXISTS `ap_condominio`;
CREATE TABLE IF NOT EXISTS `ap_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_administradora` int(11) DEFAULT NULL,
  `nomeCond` varchar(255) NOT NULL DEFAULT '',
  `qtdeBlocos` int(11) NOT NULL DEFAULT 0,
  `logradouro` varchar(255) NOT NULL DEFAULT '',
  `numero` varchar(10) NOT NULL DEFAULT '0',
  `bairro` varchar(255) NOT NULL DEFAULT '',
  `cidade` varchar(255) NOT NULL DEFAULT '',
  `estado` varchar(50) NOT NULL DEFAULT '',
  `cep` varchar(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_ap_condominio_ap_administradora` (`from_administradora`),
  CONSTRAINT `FK_ap_condominio_ap_administradora` FOREIGN KEY (`from_administradora`) REFERENCES `ap_administradora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_condominio: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_condominio` DISABLE KEYS */;
INSERT INTO `ap_condominio` (`id`, `from_administradora`, `nomeCond`, `qtdeBlocos`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`) VALUES
	(1, 1, 'Franca', 3, 'Hermann Mathes', '271', 'Vila Nova', 'Blumenau', 'PR', '89035030'),
	(2, 1, 'Bela Vista', 3, 'Rua Bahia', '565', 'Centro', 'Indaial', 'RJ', '23563560'),
	(3, 1, 'Ibis', 2, '7 de setembro', '29', 'Garcia', 'Gaspar', 'SC', '69852541'),
	(4, 1, 'Buster', 2, 'Rua LeÃ´nidas de Barros', '526', 'Jardim Santos Dumont', 'ParanavaÃ­', 'PR', '87706150'),
	(5, 2, 'Cap', 2, 'Rua Augusta Lacerda', '7452 ', 'Colinas de Indaiatuba', 'Indaiatuba', 'SP', '13331215'),
	(6, 2, 'Apogeu', 3, 'Rua Muritinga', '1076 ', 'Planalto', 'Natal', 'RN', '59073131'),
	(7, 2, 'Gloria', 2, 'Quadra P-15', '2814 ', 'TapanÃ£ (Icoaraci)', 'BelÃ©m', 'PA', '66825249'),
	(8, 2, 'D2 condominio', 5, 'Quadra 4 LE 6', '1015 ', 'Sobradinho', 'BrasÃ­lia', 'DF', '73026132'),
	(9, 3, 'Olav Bilac', 4, 'Travessa Jorge Leopoldo', '4190 ', 'Pedrinhas', 'Picos', 'PI', '64605035'),
	(10, 3, 'Villandry', 3, 'Rua Reverendo Joaquim dos Santos Oliveira', '7045 ', 'Atuba', 'Curitiba', 'PR', '82860100');
/*!40000 ALTER TABLE `ap_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_sistema.ap_conselho
DROP TABLE IF EXISTS `ap_conselho`;
CREATE TABLE IF NOT EXISTS `ap_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `telefone` varchar(11) NOT NULL DEFAULT '',
  `funcao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ap_conselho_ap_condominio` (`from_condominio`),
  CONSTRAINT `FK_ap_conselho_ap_condominio` FOREIGN KEY (`from_condominio`) REFERENCES `ap_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_conselho: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_conselho` DISABLE KEYS */;
INSERT INTO `ap_conselho` (`id`, `from_condominio`, `nome`, `cpf`, `telefone`, `funcao`) VALUES
	(1, 1, 'Maria', '12345678910', '47997760344', 'sindico'),
	(2, 1, 'Diego', '10987654321', '47965214523', 'subsindico'),
	(3, 1, 'Clara', '65432198750', '47954178563', 'conselheiro'),
	(4, 1, 'Carla', '54578965852', '47965893256', 'conselheiro'),
	(5, 1, 'Rodrigo', '45887455454', '47965410258', 'conselheiro'),
	(6, 2, 'Luiza', '45887455454', '47965410258', 'sindico'),
	(7, 2, 'Ana', '45887455454', '47965410258', 'subsindico'),
	(8, 2, 'Rita', '45887455454', '47965410258', 'conselheiro'),
	(9, 2, 'Jose', '45887455454', '47965410258', 'conselheiro'),
	(10, 2, 'Elias', '45887455454', '47965410258', 'conselheiro'),
	(11, 3, 'Luana', '54687598620', '54965425874', 'sindico'),
	(12, 3, 'Vivian', '56983654212', '48965425874', 'subsindico'),
	(13, 3, 'Ruan', '56983654212', '48965425874', 'conselheiro'),
	(14, 3, 'Bruna', '54568752168', '45965875455', 'conselheiro'),
	(15, 3, 'Marcos', '54568752168', '45965875455', 'conselheiro'),
	(16, 4, 'Vinicius', '54878554255', '54863542248', 'sindico'),
	(17, 4, 'Peror', '54878554255', '54863542248', 'subsindico'),
	(18, 4, 'Xetiubir', '54878554255', '54863542248', 'conselheiro'),
	(19, 4, 'Noanis', '54878554255', '54863542248', 'conselheiro'),
	(20, 4, 'Blepo', '54878554255', '54863542248', 'conselheiro'),
	(21, 5, 'Moaray', '54878554255', '54863542248', 'sindico'),
	(22, 5, 'Fualanr', '54878554255', '54863542248', 'subsindico'),
	(23, 5, 'Kansieir', '54878554255', '54863542248', 'conselheiro'),
	(24, 5, 'Fiousta', '54878554255', '54863542248', 'conselheiro'),
	(25, 5, 'Gipye', '54878554255', '54863542248', 'conselheiro');
/*!40000 ALTER TABLE `ap_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_sistema.ap_moradores
DROP TABLE IF EXISTS `ap_moradores`;
CREATE TABLE IF NOT EXISTS `ap_moradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL,
  `from_bloco` int(11) NOT NULL,
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `telefone` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`from_condominio`),
  KEY `chBloco` (`from_bloco`),
  KEY `chUnidade` (`from_unidade`),
  CONSTRAINT `chBloco` FOREIGN KEY (`from_bloco`) REFERENCES `ap_blocos` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `ap_condominio` (`id`),
  CONSTRAINT `chUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `ap_unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_moradores: ~46 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_moradores` DISABLE KEYS */;
INSERT INTO `ap_moradores` (`id`, `from_condominio`, `from_bloco`, `from_unidade`, `nome`, `cpf`, `telefone`, `email`, `dataCadastro`) VALUES
	(3, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(4, 10, 2, 8, 'Diego', '1234567891', '47997760344', 'diego@gmail.com', '2022-04-01 11:15:56'),
	(5, 5, 3, 6, 'Clara', '1234567891', '47997760344', 'clara@gmail.com', '2022-04-01 11:15:56'),
	(6, 8, 2, 5, 'Marcelo', '1234567891', '47997760344', 'marcelo@gmail.com', '2022-04-01 11:15:56'),
	(7, 1, 1, 1, 'Ana Rita', '1234567891', '47997760344', 'ana@gmail.com', '2022-04-01 11:46:03'),
	(10, 1, 2, 1, 'Zézinho', '451.126.541', '47997760344', 'mcfranca0107@gmail.com', '2022-04-10 12:07:58'),
	(11, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(12, 3, 28, 15, 'ZÃ©zinho AA', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:44:32'),
	(13, 1, 3, 6, 'teste', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:44:45'),
	(15, 1, 1, 18, 'Zézinho', '451.126.541', '47997760344', 'mcfranca0107@gmail.com', '2022-04-10 12:07:07'),
	(16, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(17, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(18, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(19, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(20, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(21, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(22, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(23, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(24, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(25, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(26, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(27, 9, 1, 4, 'Maria Clara', '1234567891', '47997760344', 'mcfranca0107@gmail.com', '2022-04-01 11:15:56'),
	(28, 1, 3, 3, 'Ana Rita', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:45:11'),
	(29, 1, 3, 3, 'Ana Rita', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:45:11'),
	(30, 1, 3, 3, 'Ana Rita', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:45:11'),
	(31, 1, 3, 3, 'Ana Rita', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:45:11'),
	(32, 1, 3, 3, 'Ana Rita', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 11:45:11'),
	(33, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(34, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(35, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(36, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(37, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(38, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(39, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(40, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(41, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(42, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(43, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(44, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(45, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(46, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(47, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(48, 3, 28, 15, 'ZÃ©zinho', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-04 10:11:25'),
	(50, 1, 3, 8, 'Zézinho', '451.126.541', '47997760344', 'mcfranca0107@gmail.com', '2022-04-10 12:08:16'),
	(51, 3, 28, 15, 'maria', '45112654123', '47997760344', 'mcfranca0107@gmail.com', '2022-04-06 14:40:19'),
	(52, 1, 2, 5, 'Zézinho AA', '451.126.541', '47997760344', 'mcfranca0107@gmail.com', '2022-04-10 12:07:29'),
	(53, 1, 1, 2, 'Cezar Augusto', '123.456.789', '47337760344', 'cezar@gmail.com', '2022-04-10 12:05:10');
/*!40000 ALTER TABLE `ap_moradores` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_sistema.ap_unidades
DROP TABLE IF EXISTS `ap_unidades`;
CREATE TABLE IF NOT EXISTS `ap_unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_blocoUni` int(11) NOT NULL DEFAULT 0,
  `from_condBloco` int(11) NOT NULL,
  `numUnidade` int(11) NOT NULL DEFAULT 0,
  `metroUnidade` float NOT NULL DEFAULT 0,
  `qtdeVagas` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_ap_unidades_ap_blocos` (`from_blocoUni`),
  KEY `FK_ap_unidades_ap_condominio` (`from_condBloco`),
  CONSTRAINT `FK_ap_unidades_ap_blocos` FOREIGN KEY (`from_blocoUni`) REFERENCES `ap_blocos` (`id`),
  CONSTRAINT `FK_ap_unidades_ap_condominio` FOREIGN KEY (`from_condBloco`) REFERENCES `ap_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_unidades: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_unidades` DISABLE KEYS */;
INSERT INTO `ap_unidades` (`id`, `from_blocoUni`, `from_condBloco`, `numUnidade`, `metroUnidade`, `qtdeVagas`, `dataCadastro`) VALUES
	(1, 2, 7, 101, 54.3, 2, '2022-03-30 15:01:27'),
	(2, 1, 8, 102, 60, 1, '2022-03-30 15:01:28'),
	(3, 3, 2, 103, 49.5, 2, '2022-03-30 15:01:31'),
	(4, 1, 10, 104, 49.5, 2, '2022-03-30 15:01:34'),
	(5, 2, 5, 104, 49.5, 2, '2022-03-30 15:01:37'),
	(6, 3, 2, 104, 49.5, 2, '2022-03-30 15:00:49'),
	(7, 1, 1, 104, 49.5, 2, '2022-03-30 14:04:31'),
	(8, 3, 2, 104, 49.5, 2, '2022-03-30 15:00:52'),
	(15, 28, 3, 1111, 54, 2, '2022-04-04 10:11:06'),
	(18, 1, 1, 101, 54, 4, '2022-04-04 11:32:53');
/*!40000 ALTER TABLE `ap_unidades` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_sistema.ap_usuarios
DROP TABLE IF EXISTS `ap_usuarios`;
CREATE TABLE IF NOT EXISTS `ap_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `usuario` varchar(255) NOT NULL DEFAULT '0',
  `senha` varchar(255) NOT NULL DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_sistema.ap_usuarios: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_usuarios` DISABLE KEYS */;
INSERT INTO `ap_usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(21, 'Maria Clara', 'Maria Clara', '202cb962ac59075b964b07152d234b70', '2022-04-10 11:52:54'),
	(22, 'Diego Bracellos', 'Diego', '202cb962ac59075b964b07152d234b70', '2022-04-10 11:22:10'),
	(24, 'Cezar', 'Cezinha', '202cb962ac59075b964b07152d234b70', '2022-04-10 12:01:28');
/*!40000 ALTER TABLE `ap_usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
