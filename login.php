<?
//error_reporting('E_FATAL | E_PARSE' );
require "uteis.php";
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/login.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Gestão de Clientes</title>
</head>

<body class="login">
    <img src="imagens/logo.png" alt="">
    <main class="container">
        <div class="row">
            <div class="caixaLogin">
                <form class="form-signin" action="<?=$url_site?>controllers/restrito.php" method="POST">
                    <h1 class="h3 mb-3"><b>Área de login</b></h1>
                    
                    <label for="usuario" class="sr-only">Usuário</label>
                    <input type="text" id="usuario" class="form-control mb-3" name="usuario" placeholder="Usuário" required autofocus>
                    
                    <label for="senha" class="sr-only">Senha</label>
                    <input type="password" id="senha" class="form-control mb-3" name="senha" placeholder="Senha" required>
                    
                    <button class="btn btn-lg btn-dark btn-block" type="submit">Entrar</button>
                </form>
            </div>
        </div>
    </main>

    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.bundle.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/app.js?v=<?=rand(0,9999)?>"></script>
    <? if(isset($_GET['msg'])){?>
        <script type="text/javascript">
            $(function(){
                myAlert('danger', '<?=$_GET['msg']?>', 'form');
            })
        </script>
    <?}?>
</body>

</html>