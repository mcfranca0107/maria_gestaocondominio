<?
require "../uteis.php";

$usuarios = new cadPrincipal();
$result = $usuarios->deletaUsuario($_POST['id']);

if($result){
    $totalRegistros = $usuarios->getUsuario()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso.",
    );

    echo json_encode($result);

} else{

    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado.",
    );

    echo json_encode($result);
}


?>