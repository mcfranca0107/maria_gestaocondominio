<?
require "../uteis.php";

$blocos = new CadBlocos();

if($blocos -> editBloco($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Registro editado com sucesso.",
    );

    echo json_encode($result);

} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser editado.",
    );

    echo json_encode($result);
};

?>