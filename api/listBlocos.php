<?
require "../uteis.php";

$blocos = new cadBlocos();
$dados = $blocos->getBlocoFromCond($_REQUEST['id']);

if(!empty($dados)){
    $result = array(
        "status" => 'success',
        "resultSet" => $dados['resultSet']
    );

} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O cadastro não pode ser inserido."
    );
}

echo json_encode($result);

?>