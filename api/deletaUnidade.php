<?
require "../uteis.php";

$unidades = new CadUnidades();
$result = $unidades->deletaUnidade($_POST['id']);

if($result){
    $totalRegistros = $unidades->getDadosUni()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso.",
    );

    echo json_encode($result);

} else{

    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado.",
    );

    echo json_encode($result);
}


?>