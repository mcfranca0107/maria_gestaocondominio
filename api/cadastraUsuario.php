<?
require "../uteis.php";

$user = new cadPrincipal();
$isExists = $user->userExistis($_POST['usuario']);

if($isExists['resultSet']['usuario']){
    $result = array(
        "status" => "warning",
        "msg" => "Este usuário já existe"
    );

    echo json_encode($result);
    exit;
}

if($user -> setUsuario($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Registro inserido com sucesso.",
    );

    echo json_encode($result);

} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser inserido.",
    );

    echo json_encode($result);
};

?>