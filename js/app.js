$(function(){

    $('#formCadastros').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaMorador.php';
            urlRedir =  url_site+'consulta';
        } else{
            url = url_site+'api/cadastraMorador.php';
            urlRedir = url_site+'cadastro';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    });
    
    $('#listaMorador').on('click', '.removerMorador', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaMorador.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consulta');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formCondominio').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaCondominio.php';
            urlRedir = url_site+'consultaCond';
        } else{
            url = url_site+'api/cadastraCond.php';
            urlRedir = url_site+'cadCondominio';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaCond').on('click', '.removerCond', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaCond');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formBlocos').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaBloco.php';
            urlRedir = url_site+'consultaBlocos';
        } else{
            url = url_site+'api/cadastraBloco.php';
            urlRedir = url_site+'cadBlocos';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaBloco').on('click', '.removerBloco', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaBlocos');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formUnidades').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaUnidade.php';
            urlRedir = url_site+'consultaUni';
        } else{
            url = url_site+'api/cadastraUnidade.php';
            urlRedir = url_site+'cadUnidades';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaUni').on('click', '.removerUni', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaUni');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaConselho.php';
            urlRedir = url_site+'consultaConselho';
        } else{
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'cadConselho';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaConselho').on('click', '.removerConselho', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaConselho');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formAdm').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaAdm.php';
            urlRedir = url_site+'consultaAdm';
        } else{
            url = url_site+'api/cadastraAdm.php';
            urlRedir = url_site+'cadAdm';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaAdm').on('click', '.removerAdm', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaAdm.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaAdm');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formCadastro').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        var senha = $(this).find('input[name="senha"]').val();
        var confirmaSenha = $(this).find('input[name="confSenha"]').val();

        if(senha == confirmaSenha){
            if(editar){
                url = url_site+'api/editaUsuario.php';
                urlRedir = url_site+'consultaUser';
            } else{
                url = url_site+'api/cadastraUsuario.php';
                urlRedir = url_site+'cadastroPrincipal';
            }
    
            $('.btnEnviar').attr('disabled', 'disabled');        
    
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data){
                    if(data.status == 'success'){
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    } else{
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    }
                }
            });
        } else{
            myAlert('danger','Senhas não conferem.', 'main');
        }

        return false;
    })

    $('#listaUser').on('click', '.removerUsuario', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: url_site+'api/deletaUsuario.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaUser');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    //chamar valores para selects filhos
    $('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType:'json',
            type: 'POST',
            data: { id: selecionado },
            success: function(data){
                console.log(data);

                selectPopulation('.fromBloco',data.resultSet,'nomeBloco');
            }
        })
    })

    
    $('.fromBloco').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType:'json',
            type: 'POST',
            data: { id: selecionado },
            success: function(data){
                selectPopulation('.fromUnidade',data.resultSet,'numUnidade');
            }
        })
    })
    
    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Select</option>';

        for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value="' + dados[i].id + '">' + dados[i][field] + '</option>';
        }

        $(seletor).html(estrutura)
    }


    // Controlador do filtro
    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : '';

        window.location.href = url_site+pagina+'/busca/'+termo1+termo2;

        return false;
    })

    $('.termo1, .termo2').on('keyup focusout change', function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled', false);
        } else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })
});

//exemplo de como usar plugin de mascara nos campos
$('input[name="cpf"]').mask('999.999.999-99', {reverse:true});

function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url == '' : url = url;
    componente = '<div class="alert alert-'+ tipo +'" role="alert">'+mensagem+'</div>';

    $(pai).prepend(componente);

    setTimeout(function(){
        $(pai).find('div.alert').remove();

        //vai redirecionar?
        if(tipo == 'success' && url){
            setTimeout(function(){
                window.location.href = url;
            }, 500);
        }

    }, 1000)
}
